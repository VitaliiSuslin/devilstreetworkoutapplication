import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.1.8.RELEASE"
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    kotlin("jvm") version "1.2.71"
    kotlin("plugin.spring") version "1.2.71"
    kotlin("plugin.jpa") version "1.2.71"
}

group = "com.devilstreetworkout"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenCentral()
}

extra["springBootAdminVersion"] = "2.1.5"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")

    implementation("org.springframework.security:spring-security-jwt:1.0.9.RELEASE")
    implementation("io.jsonwebtoken:jjwt:0.9.0")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("de.codecentric:spring-boot-admin-starter-server")
//    implementation("de.codecentric:spring-boot-admin-starter-client")

    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("io.reactivex.rxjava3:rxjava:3.0.0-RC3")

    implementation("io.springfox:springfox-swagger2:2.6.1")
    implementation("io.springfox:springfox-swagger-ui:2.6.1")

    runtimeOnly("mysql:mysql-connector-java")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

dependencyManagement {
    imports {
        mavenBom("de.codecentric:spring-boot-admin-dependencies:${property("springBootAdminVersion")}")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}
//tasks.getByName<Jar>("jar") {
//    isEnabled = true
//}
//
//tasks.getByName<BootJar>("bootJar") {
//    archiveClassifier.set("boot")
//    mainClassName ="com.devilstreetworkout.devilstreetworkout.DevilstreetworkoutApplication.kt"
//}
