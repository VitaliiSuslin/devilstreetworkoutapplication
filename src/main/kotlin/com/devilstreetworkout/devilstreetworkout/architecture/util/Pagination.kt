package com.devilstreetworkout.devilstreetworkout.architecture.util

fun startFrom(pageIndex: Long, pageSize: Long): Long {
    val result = ((pageIndex - 1L) * pageSize) + 1L
    return if (result > 0) result else 0
}
