package com.devilstreetworkout.devilstreetworkout.architecture.util

data class Optional<T>(var data: T?)