package com.devilstreetworkout.devilstreetworkout.architecture.expectedTrouble

import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException

class BadRequestException(override val message: String) : HttpClientErrorException(HttpStatus.BAD_REQUEST)