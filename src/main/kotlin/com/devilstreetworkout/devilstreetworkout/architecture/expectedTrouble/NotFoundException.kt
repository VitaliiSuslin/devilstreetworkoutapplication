package com.devilstreetworkout.devilstreetworkout.architecture.expectedTrouble

import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException

class NotFoundException(override val message: String?) : HttpClientErrorException(HttpStatus.NOT_FOUND)