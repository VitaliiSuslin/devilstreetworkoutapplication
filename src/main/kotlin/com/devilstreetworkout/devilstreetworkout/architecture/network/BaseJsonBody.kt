package com.devilstreetworkout.devilstreetworkout.architecture.network

import com.fasterxml.jackson.annotation.JsonCreator

abstract class BaseJsonBody @JsonCreator constructor()