package com.devilstreetworkout.devilstreetworkout.architecture.network

import com.fasterxml.jackson.annotation.JsonProperty

abstract class BaseResponse(
        @JsonProperty("code")
        var code: Int? = null
) : BaseJsonBody()