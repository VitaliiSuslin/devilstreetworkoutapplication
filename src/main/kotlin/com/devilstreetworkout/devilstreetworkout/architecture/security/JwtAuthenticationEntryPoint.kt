package com.devilstreetworkout.devilstreetworkout.architecture.security

import org.slf4j.LoggerFactory
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthenticationEntryPoint : AuthenticationEntryPoint {
    override fun commence(request: HttpServletRequest?, response: HttpServletResponse?, authException: AuthenticationException?) {
        LoggerFactory.getLogger(this::class.java).error("Responding with unauthorized error. Message -{}", authException?.message)
        response?.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Sorry, You`re not authorized to access the resource")
    }
}