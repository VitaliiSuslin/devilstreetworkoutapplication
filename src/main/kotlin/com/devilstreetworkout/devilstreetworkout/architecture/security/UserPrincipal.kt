package com.devilstreetworkout.devilstreetworkout.architecture.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class UserPrincipal(
        var id: Long,
        var email: String,
        var userPassword: String,
        var enable: Boolean,
        var credentialsNonExpired: Boolean,
        var accountNonExpired: Boolean,
        var accountNonLocked: Boolean,
        var authorityList: MutableCollection<GrantedAuthority>
) : UserDetails {
    override fun isEnabled(): Boolean = enable
    override fun getUsername(): String = email
    override fun isCredentialsNonExpired(): Boolean = credentialsNonExpired
    override fun getPassword(): String = userPassword
    override fun isAccountNonExpired(): Boolean = accountNonExpired
    override fun isAccountNonLocked(): Boolean = accountNonLocked
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = authorityList
}