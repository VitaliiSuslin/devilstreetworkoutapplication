package com.devilstreetworkout.devilstreetworkout.architecture.security

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.impl.DefaultClaims
import org.springframework.security.core.Authentication
import reactor.core.publisher.Mono
import java.util.*


/**
 * Container about token validation info
 * */
data class TokenValidationInfo(
        var isValid: Boolean,
        var message: String? = null
)

//@Component
//open class JwtSettings {
//    /**
//     * Look ${application.jwtSecret} at application.properties
//     */
//    @Value(value = "#{jwtSecret}")
//    public lateinit var jwtSecret: String
//
//    /**
//     * Look ${application.jwtExpirationInMs} at application.properties
//     */
//    @Value(value = "#{jwtExpirationInMs}")
//   public var jwtExpirationInMs: Long = 0
//}

var jwtSecret: String = "JWTSuperSecretKey"
var jwtExpirationInMs: Long = 60480000
/**
 * @param authentication
 * @return token [String]
 * */
fun generateToken(authentication: Authentication): String {
    val userPrincipal = authentication.principal as UserPrincipal
    val currentDate = Date()
    val expirationDate = Date(currentDate.time + jwtExpirationInMs)

    return Jwts
            .builder()
            .setSubject(userPrincipal.id.toString())
            .setIssuedAt(currentDate)
            .setExpiration(expirationDate)
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact()
}

/**
 * Parsing jwt and getting Claims
 * @param token [String]
 * @return claims [DefaultClaims]
 * */
@Throws(Exception::class)
fun getUserClaimsFromJWT(token: String): DefaultClaims {
    return Jwts.parser()
            .setSigningKey(jwtSecret)
            .parse(token)
            .body as DefaultClaims
}

/**
 * Parsing jwt and getting Claims
 * @param token [String]
 * @return userId [Long] or <b>null</b>
 * */
fun getUserIdFromJWT(token: String): Long? {
    return (Jwts.parser()
            .setSigningKey(jwtSecret)
            .parse(token)
            .body as? DefaultClaims)?.subject?.toLong()
}

/**
 * Async token validation
 * @param token
 * @return [Mono]
 * */
fun validateToken(token: String): TokenValidationInfo {
    return try {
        Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token)
        TokenValidationInfo(
                isValid = true
        )
    } catch (ex: Exception) {
        TokenValidationInfo(
                isValid = false,
                message = ex.message
        )
    }
}

/**
 * Parse header and return token or null
 * @param header [String]
 * */
fun getJwtFromHeader(header: String): String? {
    return if (header.startsWith("Bearer ")) header.substring(7) else null
}
