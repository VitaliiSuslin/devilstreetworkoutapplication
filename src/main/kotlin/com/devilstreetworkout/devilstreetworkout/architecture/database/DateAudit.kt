package com.devilstreetworkout.devilstreetworkout.architecture.database

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.time.Instant
import javax.persistence.Column
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class DateAudit : Serializable {
    @CreatedDate
    @Column(name = "create_at", nullable = false, updatable = false)
    lateinit var createAr: Instant

    @LastModifiedDate
    @Column(name = "update_at", nullable = false)
    lateinit var updateAt: Instant
}