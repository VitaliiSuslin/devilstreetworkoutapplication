package com.devilstreetworkout.devilstreetworkout.trainProgram.network.response

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.dto.TrainProgramDto
import com.fasterxml.jackson.annotation.JsonProperty

data class GetTrainProgramsResponse(
        @JsonProperty("total_size")
        var totalSize: Long,
        @JsonProperty("train_program")
        var trainPrograms: MutableList<TrainProgramDto>
) : BaseResponse()