package com.devilstreetworkout.devilstreetworkout.trainProgram.network.response

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse

class CreateTrainProgramResponse() : BaseResponse()