package com.devilstreetworkout.devilstreetworkout.trainProgram.network.request.dto

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseJsonBody
import com.fasterxml.jackson.annotation.JsonProperty

data class ExerciseDto(
        @JsonProperty("title")
        var title: String,

        @JsonProperty("description")
        var description: String
) : BaseJsonBody()