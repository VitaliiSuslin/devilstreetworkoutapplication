package com.devilstreetworkout.devilstreetworkout.trainProgram.network.response

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.dto.TrainProgramDto
import com.fasterxml.jackson.annotation.JsonProperty

data class GetTrainProgramResponse(
        @JsonProperty("train_program")
        var trainProgram: TrainProgramDto
) : BaseResponse()