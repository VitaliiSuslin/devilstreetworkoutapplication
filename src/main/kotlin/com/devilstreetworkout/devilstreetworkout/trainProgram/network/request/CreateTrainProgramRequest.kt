package com.devilstreetworkout.devilstreetworkout.trainProgram.network.request

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseJsonBody
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.request.dto.TrainProgramDto
import com.fasterxml.jackson.annotation.JsonProperty

data class CreateTrainProgramRequest(
        @JsonProperty("train_program")
        var trainProgram: TrainProgramDto
) : BaseJsonBody()