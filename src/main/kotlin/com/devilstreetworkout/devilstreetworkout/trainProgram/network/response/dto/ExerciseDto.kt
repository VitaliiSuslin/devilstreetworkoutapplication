package com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.dto

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseJsonBody
import com.fasterxml.jackson.annotation.JsonProperty

data class ExerciseDto(
        @JsonProperty("exercise_id")
        var id: Long,

        @JsonProperty("title")
        var title: String,

        @JsonProperty("description")
        var description: String
) : BaseJsonBody()