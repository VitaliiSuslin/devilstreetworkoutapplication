package com.devilstreetworkout.devilstreetworkout.trainProgram.network.controller

import com.devilstreetworkout.devilstreetworkout.ROUT_API
import com.devilstreetworkout.devilstreetworkout.ioSchedule
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.request.CreateTrainProgramRequest
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.CreateTrainProgramResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.GetTrainProgramResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.GetTrainProgramsResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.service.TrainProgramService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import javax.validation.Valid

@RestController
@RequestMapping("$ROUT_API/train_program")
class TrainProgramController
constructor(
        val trainProgramService: TrainProgramService
) {

    @PostMapping
    fun createTrainProgram(
            @RequestBody
            @Valid
            request: CreateTrainProgramRequest
    ): Mono<ResponseEntity<CreateTrainProgramResponse>> {
        return trainProgramService
                .createTrainProgram(request)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }

    @GetMapping("page_index={page_index}&page_size={page_size}")
    fun getTrainPrograms(
            @PathVariable("page_index")
            pageIndex: Long,
            @PathVariable("page_size")
            pageSize: Long

    ): Mono<ResponseEntity<GetTrainProgramsResponse>> {
        return trainProgramService
                .getTrainPrograms(pageIndex,pageSize)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }

    @GetMapping("train_program_id={id}")
    fun getTrainProgram(@PathVariable("id") id: Long): Mono<ResponseEntity<GetTrainProgramResponse>> {
        return trainProgramService
                .getTrainProgram(id)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }
}