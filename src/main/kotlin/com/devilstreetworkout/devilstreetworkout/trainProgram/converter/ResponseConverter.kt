package com.devilstreetworkout.devilstreetworkout.trainProgram.converter

import com.devilstreetworkout.devilstreetworkout.trainProgram.database.entity.DbExercise
import com.devilstreetworkout.devilstreetworkout.trainProgram.database.entity.DbTrainProgram
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.dto.ExerciseDto
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.dto.TrainProgramDto

fun DbTrainProgram.toTrainProgramDto(): TrainProgramDto {
    return TrainProgramDto(
            id,
            title,
            description,
            exercises?.map { it.toExerciseDto() }?.toMutableList() ?: mutableListOf()
    )
}

fun DbExercise.toExerciseDto(): ExerciseDto {
    return ExerciseDto(id, title, description)
}