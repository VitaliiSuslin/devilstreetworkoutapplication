package com.devilstreetworkout.devilstreetworkout.trainProgram.converter

import com.devilstreetworkout.devilstreetworkout.trainProgram.database.entity.DbExercise
import com.devilstreetworkout.devilstreetworkout.trainProgram.database.entity.DbTrainProgram
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.request.CreateTrainProgramRequest
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.request.dto.ExerciseDto
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.request.dto.TrainProgramDto

fun CreateTrainProgramRequest.toDbTrainProgram(): DbTrainProgram {
    return trainProgram.toDbTrainProgram()
}

fun TrainProgramDto.toDbTrainProgram(): DbTrainProgram {
    return DbTrainProgram(title, description, ArrayList(exercises.map { it.toDbExercise() }))
}

fun ExerciseDto.toDbExercise(): DbExercise {
    return DbExercise(title, description)
}
