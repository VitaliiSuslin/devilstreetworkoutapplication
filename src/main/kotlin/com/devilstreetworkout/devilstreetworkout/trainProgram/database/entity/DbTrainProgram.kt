package com.devilstreetworkout.devilstreetworkout.trainProgram.database.entity

import com.devilstreetworkout.devilstreetworkout.GENERATOR_NATIVE
import com.devilstreetworkout.devilstreetworkout.architecture.database.DateAudit
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
@Table(name = "train_programs")
data class DbTrainProgram(
        @Column(
                name = "title",
                length = 512,
                nullable = false
        )
        var title: String,

        @Column(
                name = "description",
                columnDefinition = "TEXT"
        )
        var description: String,

        @OneToMany(
                fetch = FetchType.EAGER,
                targetEntity = DbExercise::class,
                cascade = [CascadeType.PERSIST, CascadeType.REFRESH]
        )
        var exercises: MutableList<DbExercise>? = null,

        @Id
        @GeneratedValue(
                strategy = GenerationType.AUTO,
                generator = GENERATOR_NATIVE
        )
        @GenericGenerator(
                name = GENERATOR_NATIVE,
                strategy = GENERATOR_NATIVE
        )
        @Column(
                name = "train_program_id",
                updatable = false,
                nullable = false
        )
        var id: Long = 0
) : DateAudit()