package com.devilstreetworkout.devilstreetworkout.trainProgram.database.repository

import com.devilstreetworkout.devilstreetworkout.trainProgram.database.entity.DbExercise
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ExercisesRepository : JpaRepository<DbExercise, Long>