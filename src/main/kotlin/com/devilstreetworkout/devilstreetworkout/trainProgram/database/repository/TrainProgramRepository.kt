package com.devilstreetworkout.devilstreetworkout.trainProgram.database.repository

import com.devilstreetworkout.devilstreetworkout.trainProgram.database.entity.DbTrainProgram
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface TrainProgramRepository : JpaRepository<DbTrainProgram, Long> {

    @Query("SELECT * FROM `train_programs` LIMIT :limit OFFSET :startFrom",
            nativeQuery = true)
    fun findAll(
            @Param("limit")
            limit: Long,
            @Param("startFrom")
            startFrom: Long
    ): List<DbTrainProgram>
}