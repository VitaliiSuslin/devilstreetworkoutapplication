package com.devilstreetworkout.devilstreetworkout.trainProgram.database.entity

import com.devilstreetworkout.devilstreetworkout.GENERATOR_NATIVE
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
@Table(name = "exercises")
data class DbExercise(
        @Column(
                name = "title",
                length = 512,
                nullable = false
        )
        var title: String,

        @Column(
                name = "description",
                columnDefinition = "TEXT"
        )
        var description: String,

        @ManyToOne(
                targetEntity = DbTrainProgram::class,
                cascade = [CascadeType.PERSIST, CascadeType.REFRESH]
        )
        var trainPrograms: DbTrainProgram? = null,

        @Id
        @GeneratedValue(
                strategy = GenerationType.AUTO,
                generator = GENERATOR_NATIVE
        )
        @GenericGenerator(
                name = GENERATOR_NATIVE,
                strategy = GENERATOR_NATIVE
        )
        @Column(name = "exercise_id", updatable = false, nullable = false)
        var id: Long = 0
)