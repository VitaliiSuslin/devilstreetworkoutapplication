package com.devilstreetworkout.devilstreetworkout.trainProgram.service

import com.devilstreetworkout.devilstreetworkout.trainProgram.network.request.CreateTrainProgramRequest
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.CreateTrainProgramResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.GetTrainProgramResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.GetTrainProgramsResponse
import reactor.core.publisher.Mono

interface TrainProgramService {
    fun createTrainProgram(request: CreateTrainProgramRequest): Mono<CreateTrainProgramResponse>

    fun getTrainPrograms(pageIndex: Long, pageSize: Long): Mono<GetTrainProgramsResponse>

    fun getTrainProgram(trainProgramId: Long): Mono<GetTrainProgramResponse>
}