package com.devilstreetworkout.devilstreetworkout.trainProgram.service.impl

import com.devilstreetworkout.devilstreetworkout.architecture.util.startFrom
import com.devilstreetworkout.devilstreetworkout.trainProgram.converter.toDbTrainProgram
import com.devilstreetworkout.devilstreetworkout.trainProgram.converter.toTrainProgramDto
import com.devilstreetworkout.devilstreetworkout.trainProgram.database.repository.TrainProgramRepository
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.request.CreateTrainProgramRequest
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.CreateTrainProgramResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.GetTrainProgramResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.network.response.GetTrainProgramsResponse
import com.devilstreetworkout.devilstreetworkout.trainProgram.service.TrainProgramService
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import javax.transaction.Transactional

@Service
class TrainProgramServiceImpl constructor(
        val trainProgramRepository: TrainProgramRepository
) : TrainProgramService {

    @Transactional
    override fun createTrainProgram(request: CreateTrainProgramRequest): Mono<CreateTrainProgramResponse> {
        return Mono.fromCallable {
            request.toDbTrainProgram()
        }
                .flatMap { trainProgram ->
                    trainProgramRepository.save(trainProgram)
                    Mono.just(CreateTrainProgramResponse())
                }
    }

    override fun getTrainPrograms(pageIndex: Long, pageSize: Long): Mono<GetTrainProgramsResponse> {
        return Mono.fromCallable {
            trainProgramRepository.findAll(pageSize, startFrom(pageIndex, pageSize))
        }.flatMap { trainPrograms ->
            Mono.just(
                    GetTrainProgramsResponse(
                            trainProgramRepository.count(),
                            trainPrograms.map { it.toTrainProgramDto() }.toMutableList()
                    )
            )
        }
    }

    override fun getTrainProgram(trainProgramId: Long): Mono<GetTrainProgramResponse> {
        return Mono.fromCallable {
            trainProgramRepository.findById(trainProgramId)
        }
                .flatMap { container ->
                    if (container.isPresent) {
                        Mono.just(GetTrainProgramResponse(container.get().toTrainProgramDto()))
                    } else {
                        Mono.error(NotFoundException())
                    }
                }
    }
}