package com.devilstreetworkout.devilstreetworkout

import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers

// ROUT_API = /api
const val ROUT_API = "/api"

//Reactive const
val ioSchedule: Scheduler = Schedulers.elastic()
val singleScheduler: Scheduler = Schedulers.single()

// Generators
const val GENERATOR_NATIVE = "native"

// Authorization
const val HEADER_AUTHORIZATION = "Authorization"