package com.devilstreetworkout.devilstreetworkout.exceptionHendler

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse
import com.fasterxml.jackson.annotation.JsonProperty

data class ExceptionResponse(

        @JsonProperty("request_description")
        var requestDescription: String,

        @JsonProperty("code")
        var errorCode: Int,

        @JsonProperty("message")
        var message: String? = null
) : BaseResponse()