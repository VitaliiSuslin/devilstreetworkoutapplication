package com.devilstreetworkout.devilstreetworkout.exceptionHendler

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionControllerAdvice : ResponseEntityExceptionHandler() {
    @ExceptionHandler(
            value = [
                IllegalArgumentException::class,
                IllegalStateException::class
            ]
    )
    protected fun handleConflict(exception: RuntimeException, request: WebRequest): ResponseEntity<ExceptionResponse> {
        return ResponseEntity(
                ExceptionResponse(
                        request.getDescription(false),
                        HttpStatus.CONFLICT.value(),
                        exception.message
                ),
                HttpStatus.CONFLICT
        )
    }

    @ExceptionHandler(
            value = [
                HttpClientErrorException::class
            ]
    )
    protected fun handleHttpClientError(exception: HttpClientErrorException, request: WebRequest): ResponseEntity<ExceptionResponse> {
        return ResponseEntity(
                ExceptionResponse(
                        request.getDescription(false),
                        exception.statusCode.value(),
                        exception.message
                ),
                exception.statusCode
        )
    }
}