package com.devilstreetworkout.devilstreetworkout.user.converter

import com.devilstreetworkout.devilstreetworkout.architecture.security.UserPrincipal
import com.devilstreetworkout.devilstreetworkout.user.database.entity.DbUser
import org.springframework.security.core.authority.SimpleGrantedAuthority

fun DbUser.toUserPrincipal(): UserPrincipal {
    return UserPrincipal(
            id,
            email,
            userPassword,
            isUserEnable,
            isCredentialsNonExpired,
            isAccountNonExpired,
            isUserNonLocke,
            roles.map { role ->
                SimpleGrantedAuthority(role.roleName.name)
            }
                    .toMutableList()
    )
}