package com.devilstreetworkout.devilstreetworkout.user.database.repositroy

import com.devilstreetworkout.devilstreetworkout.user.database.entity.DbUserDetails
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserDetailsRepository : JpaRepository<DbUserDetails, Long>