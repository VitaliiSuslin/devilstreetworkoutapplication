package com.devilstreetworkout.devilstreetworkout.user.database.view

data class UserDetailsView(
        var id: Long,
        var email: String,
        var userName: String,
        var dateBirth: Long? = null,
        var weight: Float? = null,
        var height: Float? = null
)