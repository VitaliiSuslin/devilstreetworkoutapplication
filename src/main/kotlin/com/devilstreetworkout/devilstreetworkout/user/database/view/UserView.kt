package com.devilstreetworkout.devilstreetworkout.user.database.view

import org.springframework.beans.factory.annotation.Value
import java.io.Serializable

interface UserView : Serializable {
    @Value("user_id")
    fun getId(): String?

    @Value("email")
    fun getEmail(): String?

    @Value("user_name")
    fun getUserName(): String?
}