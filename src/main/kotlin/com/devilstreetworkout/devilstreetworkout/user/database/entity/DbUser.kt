package com.devilstreetworkout.devilstreetworkout.user.database.entity

import com.devilstreetworkout.devilstreetworkout.GENERATOR_NATIVE
import com.devilstreetworkout.devilstreetworkout.architecture.database.DateAudit
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
@Table(name = "users")
data class DbUser(

        @Column(
                name = "email",
                unique = true,
                nullable = false
        )
        var email: String,

        @Column(
                name = "user_name",
                length = 512,
                nullable = false
        )
        var userName: String,

        @Column(
                name = "user_password",
                nullable = false
        )
        var userPassword: String,

        @Column(
                name = "is_account_non_locke",
                nullable = false,
                columnDefinition = "boolean default 1"
        )
        var isUserNonLocke: Boolean,

        @Column(
                name = "is_user_enable",
                nullable = false,
                columnDefinition = "boolean default 1"
        )
        var isUserEnable: Boolean,

        @Column(
                name = "is_account_non_expired",
                nullable = false,
                columnDefinition = "boolean default 1"
        )
        var isAccountNonExpired: Boolean,


        @Column(
                name = "is_credentials_non_expired",
                nullable = false,
                columnDefinition = "boolean default 1"
        )
        var isCredentialsNonExpired: Boolean,

        @ManyToMany(
                fetch = FetchType.EAGER,
                targetEntity = DbUserRole::class,
                cascade = [CascadeType.PERSIST, CascadeType.REFRESH]
        )
        var roles: Set<DbUserRole>,

        @OneToOne(
                targetEntity = DbUserDetails::class,
                cascade = [CascadeType.PERSIST, CascadeType.REFRESH]
        )
        @JoinColumn(
                name = "user_id",
                referencedColumnName = "user_id"
        )
        var userDetails: DbUserDetails,

        @Id
        @GeneratedValue(
                strategy = GenerationType.AUTO,
                generator = GENERATOR_NATIVE
        )
        @GenericGenerator(
                name = GENERATOR_NATIVE,
                strategy = GENERATOR_NATIVE
        )
        @Column(name = "user_id", updatable = false, nullable = false)
        var id: Long = 0L
) : DateAudit()