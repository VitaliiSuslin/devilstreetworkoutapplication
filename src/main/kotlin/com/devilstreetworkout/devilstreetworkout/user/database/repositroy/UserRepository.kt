package com.devilstreetworkout.devilstreetworkout.user.database.repositroy

import com.devilstreetworkout.devilstreetworkout.user.database.entity.DbUser
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : JpaRepository<DbUser, Long> {
    @Query(
            value = "SELECT u.* FROM `users` AS `u` WHERE u.`email` = :email ",
            nativeQuery = true
    )
    fun findByEmail(
            @Param("email")
            email: String
    ): Optional<DbUser>


    @Query(
            value = "SELECT * FROM `users` LIMIT :limit OFFSET :offset",
            nativeQuery = true
    )
    fun findAll(
            @Param("limit")
            limit: Long,
            @Param("offset")
            offset: Long
    ): List<DbUser>
}