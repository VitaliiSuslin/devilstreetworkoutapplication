package com.devilstreetworkout.devilstreetworkout.user.database.entity

import com.devilstreetworkout.devilstreetworkout.GENERATOR_NATIVE
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
@Table(name = "user_role")
data class DbUserRole(

        @Column(name = "name")
        @Enumerated(EnumType.STRING)
        var roleName: RoleName,

        @ManyToMany(
                fetch = FetchType.EAGER,
                targetEntity = DbUser::class,
                cascade = [CascadeType.PERSIST, CascadeType.REFRESH]
        )
        var users: Set<DbUser>? = null,

        @Id
        @GeneratedValue(
                strategy = GenerationType.AUTO,
                generator = GENERATOR_NATIVE
        )
        @GenericGenerator(
                name = GENERATOR_NATIVE,
                strategy = GENERATOR_NATIVE
        )
        @Column(name = "role_id")
        var id: Long = 0L
) {
    enum class RoleName {
        ROLE_USER,
        ROLE_ADMIN
    }
}