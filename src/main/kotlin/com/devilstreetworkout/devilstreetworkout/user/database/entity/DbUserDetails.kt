package com.devilstreetworkout.devilstreetworkout.user.database.entity

import javax.persistence.*

@Entity
@Table(name = "user_details")
data class DbUserDetails(
        @Column(name = "date_birth")
        var dateBirth: Long? = null,

        @Column(name = "weight")
        var weight: Float? = null,

        @Column(name = "height")
        var height: Float? = null,

        @OneToOne(
                targetEntity = DbUser::class,
                cascade = [CascadeType.PERSIST, CascadeType.REFRESH]
        )
        @JoinColumn(
                name = "user_id",
                referencedColumnName = "user_id"
        )
        var user: DbUser? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "user_id", updatable = false, nullable = false)
        var id: Long = 0
)