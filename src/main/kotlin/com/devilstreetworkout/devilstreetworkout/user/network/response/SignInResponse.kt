package com.devilstreetworkout.devilstreetworkout.user.network.response

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse
import com.fasterxml.jackson.annotation.JsonProperty

data class SignInResponse(
        @JsonProperty("token")
        var token: String,
        @JsonProperty("profile_user_id")
        var profileUserId: Long,
        @JsonProperty("users_total")
        var usersTotal: Long,
        @JsonProperty("train_programs_total")
        var trainProgramsTotal: Long
) : BaseResponse()