package com.devilstreetworkout.devilstreetworkout.user.network.response

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse

class SignOutResponse : BaseResponse()