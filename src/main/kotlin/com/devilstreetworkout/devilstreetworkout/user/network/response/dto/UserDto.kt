package com.devilstreetworkout.devilstreetworkout.user.network.response.dto

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseJsonBody
import com.fasterxml.jackson.annotation.JsonProperty

data class UserDto(
        @JsonProperty("user_id")
        var userId: Long,
        @JsonProperty("user_name")
        var userName: String,
        @JsonProperty("email")
        var email: String
) : BaseJsonBody()