package com.devilstreetworkout.devilstreetworkout.user.network.request

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseRequest
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class SignInRequest(
        @JsonProperty("email")
        @NotBlank
        var email: String,
        @JsonProperty("password")
        @NotBlank
        @Size(min = 6)
        var password: String
) : BaseRequest()