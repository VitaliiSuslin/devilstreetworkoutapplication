package com.devilstreetworkout.devilstreetworkout.user.network.response

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse
import com.devilstreetworkout.devilstreetworkout.user.network.response.dto.UserDto
import com.fasterxml.jackson.annotation.JsonProperty

data class GetUsersResponse(
        @JsonProperty("total_size")
        var totalsSize: Long,
        @JsonProperty("users")
        var users: MutableList<UserDto>
) : BaseResponse()