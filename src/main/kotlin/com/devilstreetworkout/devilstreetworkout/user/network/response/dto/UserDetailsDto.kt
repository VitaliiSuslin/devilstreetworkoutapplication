package com.devilstreetworkout.devilstreetworkout.user.network.response.dto

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseJsonBody
import com.fasterxml.jackson.annotation.JsonProperty

data class UserDetailsDto(
        @JsonProperty("id")
        var userId: Long,

        @JsonProperty("email")
        var email: String,

        @JsonProperty("name")
        var userName: String,

        @JsonProperty("date_birth")
        var dateBirth: Long? = null,

        @JsonProperty("weight")
        var weight: Float? = null,

        @JsonProperty("height")
        var height: Float? = null
) : BaseJsonBody()