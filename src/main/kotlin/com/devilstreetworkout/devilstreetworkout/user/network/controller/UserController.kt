package com.devilstreetworkout.devilstreetworkout.user.network.controller

import com.devilstreetworkout.devilstreetworkout.ROUT_API
import com.devilstreetworkout.devilstreetworkout.ioSchedule
import com.devilstreetworkout.devilstreetworkout.user.network.request.*
import com.devilstreetworkout.devilstreetworkout.user.network.response.*
import com.devilstreetworkout.devilstreetworkout.user.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import javax.validation.Valid

@RestController
@RequestMapping("$ROUT_API/user")
class UserController
constructor(
        val userService: UserService
) {

    @PostMapping(value = ["create"])
    fun signUp(
            @RequestBody
            @Valid
            request: CreateUserRequest): Mono<ResponseEntity<CreateUserResponse>> {
        return userService
                .createUser(request)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }


    @PostMapping(value = ["sign_in"])
    fun signInUser(
            @RequestBody
            @Valid
            request: SignInRequest
    ): Mono<ResponseEntity<SignInResponse>> {
        return userService
                .signInUser(request)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }

    @PostMapping(value = ["get_users"])
    fun getUsers(
            @RequestBody
            @Valid
            request: GetUsersRequest
    ): Mono<ResponseEntity<GetUsersResponse>> {
        return userService
                .getUsers(request)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }

    @GetMapping(value = ["get_users/page_index={page_index}&page_size={page_size}"])
    fun getUsers(
            @PathVariable("page_index")
            pageIndex: Long,
            @PathVariable("page_size")
            pageSize: Long
    ): Mono<ResponseEntity<GetUsersResponse>> {
        return userService
                .getUsers(GetUsersRequest(pageIndex, pageSize))
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }

    @PostMapping(value = ["get_user_details"])
    fun getUserDetails(
            @RequestBody
            @Valid
            request: GetUserDetailsRequest): Mono<ResponseEntity<GetUserDetailsResponse>> {
        return userService
                .getUserDetails(request)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }

    @GetMapping("get_user_details&userId={userId}")
    fun getUserDetails(
            @PathVariable("userId")
            userId: Long
    ): Mono<ResponseEntity<GetUserDetailsResponse>> {
        return userService
                .getUserDetails(GetUserDetailsRequest(userId))
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }

    @PostMapping("get_user")
    fun getUser(
            @RequestBody
            @Valid
            request: GetUserRequest
    ): Mono<ResponseEntity<GetUserResponse>> {
        return userService
                .getUser(request)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }

    @PostMapping("sign_out")
    fun signOut(
            @RequestBody
            @Valid
            request: SignOutRequest
    ): Mono<ResponseEntity<SignOutResponse>> {
        return userService
                .signOut(request)
                .subscribeOn(ioSchedule)
                .map {
                    ResponseEntity.ok(it.apply { it.code = HttpStatus.OK.value() })
                }
    }
}