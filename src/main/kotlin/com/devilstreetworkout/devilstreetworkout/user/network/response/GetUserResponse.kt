package com.devilstreetworkout.devilstreetworkout.user.network.response

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse
import com.devilstreetworkout.devilstreetworkout.user.network.response.dto.UserDto
import com.fasterxml.jackson.annotation.JsonProperty

data class GetUserResponse(
        @JsonProperty("user")
        var userDto: UserDto
) : BaseResponse()