package com.devilstreetworkout.devilstreetworkout.user.network.request

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseRequest
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.Size

data class GetUsersRequest(
        @JsonProperty("page_index")
        @Size(min = 1)
        var pageIndex: Long,
        @JsonProperty("page_size")
        var pageSize: Long
) : BaseRequest()