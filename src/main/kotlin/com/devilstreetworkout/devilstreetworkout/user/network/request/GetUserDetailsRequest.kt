package com.devilstreetworkout.devilstreetworkout.user.network.request

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseRequest
import com.fasterxml.jackson.annotation.JsonProperty

data class GetUserDetailsRequest(
        @JsonProperty(value = "user_id")
        var userId: Long
) : BaseRequest()