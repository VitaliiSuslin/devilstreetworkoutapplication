package com.devilstreetworkout.devilstreetworkout.user.network.request

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseRequest
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class CreateUserRequest(
        @JsonProperty(value = "user_name")
        @NotBlank
        @Size(min = 2)
        var userName: String,

        @JsonProperty(value = "email")
        @NotBlank
        var email: String,

        @JsonProperty(value = "password")
        @NotBlank
        @Size(min = 6)
        var password: String
) : BaseRequest()