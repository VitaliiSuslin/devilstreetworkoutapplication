package com.devilstreetworkout.devilstreetworkout.user.network.response

import com.devilstreetworkout.devilstreetworkout.architecture.network.BaseResponse
import com.devilstreetworkout.devilstreetworkout.user.network.response.dto.UserDetailsDto
import com.fasterxml.jackson.annotation.JsonProperty

data class GetUserDetailsResponse(
        @JsonProperty("user_details")
        var userDetails: UserDetailsDto
) : BaseResponse()