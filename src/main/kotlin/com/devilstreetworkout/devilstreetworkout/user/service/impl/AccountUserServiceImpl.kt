package com.devilstreetworkout.devilstreetworkout.user.service.impl

import com.devilstreetworkout.devilstreetworkout.architecture.expectedTrouble.NotFoundException
import com.devilstreetworkout.devilstreetworkout.user.converter.toUserPrincipal
import com.devilstreetworkout.devilstreetworkout.user.database.repositroy.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class AccountUserServiceImpl
constructor(
        private val userRepository: UserRepository
) : UserDetailsService {

    // For this solution username is email!!!
    override fun loadUserByUsername(username: String?): UserDetails {
        return userRepository
                .findByEmail(username!!)
                .orElseThrow {
                    NotFoundException("User by email:[ $username ] not found!")
                }
                .toUserPrincipal()
    }

}