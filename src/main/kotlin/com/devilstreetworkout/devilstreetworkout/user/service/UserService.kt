package com.devilstreetworkout.devilstreetworkout.user.service

import com.devilstreetworkout.devilstreetworkout.user.network.request.*
import com.devilstreetworkout.devilstreetworkout.user.network.response.*
import reactor.core.publisher.Mono

interface UserService {
    fun createUser(request: CreateUserRequest): Mono<CreateUserResponse>

    fun getUserDetails(request: GetUserDetailsRequest): Mono<GetUserDetailsResponse>

    fun getUser(request: GetUserRequest): Mono<GetUserResponse>

    fun signInUser(request: SignInRequest): Mono<SignInResponse>

    fun getUsers(request: GetUsersRequest): Mono<GetUsersResponse>

    fun signOut(request: SignOutRequest): Mono<SignOutResponse>
}