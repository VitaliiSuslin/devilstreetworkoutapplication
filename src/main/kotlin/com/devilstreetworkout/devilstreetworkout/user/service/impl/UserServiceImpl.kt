package com.devilstreetworkout.devilstreetworkout.user.service.impl

import com.devilstreetworkout.devilstreetworkout.architecture.expectedTrouble.BadRequestException
import com.devilstreetworkout.devilstreetworkout.architecture.expectedTrouble.NotFoundException
import com.devilstreetworkout.devilstreetworkout.architecture.security.generateToken
import com.devilstreetworkout.devilstreetworkout.architecture.util.startFrom
import com.devilstreetworkout.devilstreetworkout.trainProgram.database.repository.TrainProgramRepository
import com.devilstreetworkout.devilstreetworkout.user.database.entity.DbUser
import com.devilstreetworkout.devilstreetworkout.user.database.entity.DbUserDetails
import com.devilstreetworkout.devilstreetworkout.user.database.entity.DbUserRole
import com.devilstreetworkout.devilstreetworkout.user.database.repositroy.UserRepository
import com.devilstreetworkout.devilstreetworkout.user.network.request.*
import com.devilstreetworkout.devilstreetworkout.user.network.response.*
import com.devilstreetworkout.devilstreetworkout.user.network.response.dto.UserDetailsDto
import com.devilstreetworkout.devilstreetworkout.user.network.response.dto.UserDto
import com.devilstreetworkout.devilstreetworkout.user.service.UserService
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono

@Service
class UserServiceImpl
constructor(
        val userRepository: UserRepository,
        val trainProgramRepository: TrainProgramRepository,
        val passwordEncoder: PasswordEncoder,
        val authenticationManager: AuthenticationManager
) : UserService {

    @Transactional
    override fun createUser(request: CreateUserRequest): Mono<CreateUserResponse> {
        return Mono.fromCallable {
            userRepository.findByEmail(request.email)
        }.flatMap { dbUser ->
            if (!dbUser.isPresent) {
                userRepository.save(
                        DbUser(
                                request.email,
                                request.userName,
                                passwordEncoder.encode(request.password),
                                isUserNonLocke = true,
                                isUserEnable = true,
                                isAccountNonExpired = true,
                                isCredentialsNonExpired = true,
                                roles = setOf(DbUserRole(DbUserRole.RoleName.ROLE_USER)),
                                userDetails = DbUserDetails()))
                Mono.just(CreateUserResponse())
            } else {
                Mono.error(BadRequestException("Email [${request.email}] is already exist!"))
            }
        }
    }

    override fun getUserDetails(request: GetUserDetailsRequest): Mono<GetUserDetailsResponse> {
        return Mono.fromCallable {
            userRepository.findById(request.userId)
        }
                .flatMap { dbUser ->
                    if (dbUser.isPresent) {
                        val user = dbUser.get()
                        Mono.just(GetUserDetailsResponse(
                                UserDetailsDto(
                                        user.id,
                                        user.email,
                                        user.userName,
                                        user.userDetails.dateBirth,
                                        user.userDetails.weight,
                                        user.userDetails.height
                                )
                        ))
                    } else {
                        Mono.error(NotFoundException("User by id [${request.userId}] not found!"))
                    }
                }
    }

    override fun signInUser(request: SignInRequest): Mono<SignInResponse> {
        return Mono.fromCallable {
            userRepository.findByEmail(request.email)
        }
                .flatMap { dbUser ->
                    if (dbUser.isPresent) {
                        val token = generateToken(authenticationManager.authenticate(UsernamePasswordAuthenticationToken(request.email, request.password)))
                        val usersTotal = userRepository.count()
                        val trainProgramsTotal = trainProgramRepository.count()
                        Mono.just(SignInResponse(token, dbUser.get().id, usersTotal, trainProgramsTotal))
                    } else {
                        Mono.error(NotFoundException("User by email [${request.email}] not found!"))
                    }
                }
    }

    override fun getUsers(request: GetUsersRequest): Mono<GetUsersResponse> {
        return Mono.fromCallable {
            userRepository.findAll(request.pageSize, startFrom(request.pageIndex, request.pageSize))
        }
                .flatMap { users ->
                    Mono.just(GetUsersResponse(
                            userRepository.count(),
                            users.map { UserDto(it.id, it.userName, it.email) }.toMutableList()
                    ))
                }
    }

    override fun getUser(request: GetUserRequest): Mono<GetUserResponse> {
        return Mono.fromCallable {
            userRepository.getOne(request.userId)
        }.flatMap { user ->
            if (user != null) {
                Mono.just(GetUserResponse(UserDto(user.id, user.userName, user.email)))
            } else {
                Mono.error(NotFoundException("User by id [${request.userId}] not found!"))
            }
        }
    }

    //todo CAN EXTEND
    override fun signOut(request: SignOutRequest): Mono<SignOutResponse> {
        return Mono.just(SignOutResponse())
    }
}