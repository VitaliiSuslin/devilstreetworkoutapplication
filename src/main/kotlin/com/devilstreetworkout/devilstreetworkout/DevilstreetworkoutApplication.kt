package com.devilstreetworkout.devilstreetworkout

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DevilstreetworkoutApplication

fun main(args: Array<String>) {
    runApplication<DevilstreetworkoutApplication>(*args)
}
