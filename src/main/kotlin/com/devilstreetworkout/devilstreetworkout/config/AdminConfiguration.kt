package com.devilstreetworkout.devilstreetworkout.config

import de.codecentric.boot.admin.server.config.EnableAdminServer
import org.springframework.context.annotation.Configuration

@Configuration
@EnableAdminServer
class AdminConfiguration