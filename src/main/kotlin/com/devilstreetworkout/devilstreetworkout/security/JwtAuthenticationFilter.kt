package com.devilstreetworkout.devilstreetworkout.security

import com.devilstreetworkout.devilstreetworkout.HEADER_AUTHORIZATION
import com.devilstreetworkout.devilstreetworkout.architecture.security.getJwtFromHeader
import com.devilstreetworkout.devilstreetworkout.architecture.security.getUserIdFromJWT
import com.devilstreetworkout.devilstreetworkout.architecture.security.validateToken
import com.devilstreetworkout.devilstreetworkout.user.converter.toUserPrincipal
import com.devilstreetworkout.devilstreetworkout.user.database.repositroy.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.web.filter.OncePerRequestFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter : OncePerRequestFilter() {

    @Autowired
    lateinit var userRepository: UserRepository

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        checkAuth(request, response)
        filterChain.doFilter(request, response)
    }

    private fun checkAuth(request: HttpServletRequest, response: HttpServletResponse) {
        Optional.ofNullable(request.getHeader(HEADER_AUTHORIZATION))
                .ifPresent { authHeader ->
                    val jwt = getJwtFromHeader(authHeader)
                    if (jwt == null) {
                        response.status = 400
                        return@ifPresent
                    }

                    val userId = getUserIdFromJWT(jwt) ?: return@ifPresent
                    val userPrincipal = userRepository.getOne(userId).toUserPrincipal()
                    val token = validateToken(jwt)

                    if (userPrincipal.isAccountNonLocked && token.isValid) {
                        SecurityContextHolder.getContext().apply {
                            if (authentication == null) {
                                authentication = UsernamePasswordAuthenticationToken(
                                        userPrincipal,
                                        null,
                                        userPrincipal.authorities
                                ).apply {
                                    details = WebAuthenticationDetailsSource().buildDetails(request)
                                }
                            }
                        }
                    } else {
                        response.sendError(HttpStatus.UNAUTHORIZED.value(), token.message)
                    }
                }
    }
}